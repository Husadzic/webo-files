(function () {
    var filesToUpload = [];
    var fileInput = document.getElementById('exampleFormControlFile1');
    var ul = document.getElementById('result');
 
fileInput.onchange = function(evt){
    var files = evt.target.files;
    for(var i = 0; i<files.length; i++){
        filesToUpload.push(files[i]);            
    } 
    renderMsg(filesToUpload);
}

function renderMsg(tempFiles){
    ul.innerHTML = '';
    for (var i = 0; i < filesToUpload.length; i++){
        var li = document.createElement("li");
           var deleteBtn = document.createElement('i');
            deleteBtn.setAttribute("aria-hiden", "true");
            deleteBtn.classList.add("fa");
            deleteBtn.classList.add("fa-minus-square-o");
        li.innerHTML = "";
        var fileName = tempFiles[i].name;
        var fileSize = tempFiles[i].size;
        li.appendChild(document.createTextNode(fileName +" "+ fileSize));
        li.appendChild(deleteBtn);
        ul.appendChild(li); 
        deleteBtn.onclick = function(evt){
            deleteItem(evt)
        }
    } 
    
}

function deleteItem(i){
    console.log(i);
}

})();